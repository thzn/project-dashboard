let imagens = document.querySelectorAll('.small_img');
let Modal = document.querySelector('.modal_gallery');
let modalImg = document.querySelector('#modal_img');
let btClose = document.querySelector('#bt_close');
let srcVal = "";

for(let i = 0; i < imagens.length; i++){
    imagens[i].addEventListener('click', function(){
        srcVal = imagens[i].getAttribute('src');
        modalImg.setAttribute('src', srcVal);
        Modal.classList.toggle('modal_active');
    });
}

btClose.addEventListener('click', function(){
    Modal.classList.toggle('modal_active');
});
