let logado = false;
let mostrarformCliente = false;

if(localStorage.getItem("acesso") == "true"){
    logado = true;
    console.log("acesso realizado");
}
if(logado != true){
    alert("Desculpe, você precisa realizar o login!");
    window.location.href = "login.html";
}
function controleFormCliente(){
    mostrarformCliente = !mostrarformCliente;
    let form = document.getElementById("formCliente");
    if(mostrarformCliente){
        form.style.display = "block";
    }else{
        form.style.display = "none";
    }
}